from script.core.shapes import Shapes
shp = Shapes()
if __name__=="__main__":
    inp = input("Enter shape: ")
    if inp == "square":
        a = int(input("Side of square"))
        print(shp.square(a))
        print()
    elif inp == "circle":
        r = int(input("Radius of circle"))
        print(shp.circle(r))
    elif inp == "rectangle":
        l = int(input("Length of rectangle"))
        b = int(input("Breadth of rectangle"))
        print(shp.rectangle(l,b))
    elif inp == "triangle":
        b = int(input("Base of triangle"))
        h = int(input("Height of triangle"))
        print(shp.triangle(b,h))
    else:
        print("Shape not available")