class Shapes:
    @staticmethod
    def circle(rad):
        peri=2*3.14*rad
        area=3.14*rad*rad
        calc = dict()
        calc["area"] = area
        calc["perimeter"] = peri
        return calc

    @staticmethod
    def square(side):
        peri = 4*side
        area = side*side
        calc = dict()
        calc["area"] = area
        calc["perimeter"] = peri
        return calc

    @staticmethod
    def rectangle(len,brd):
        peri = 2*(len+brd)
        area = len*brd
        calc = dict()
        calc["area"] = area
        calc["perimeter"] = peri
        return calc

    @staticmethod
    def triangle(base,hgt):
        peri = 2*(base+hgt)
        area =0.5*base*hgt
        calc = dict()
        calc["area"] = area
        calc["perimeter"] = peri
        return calc
